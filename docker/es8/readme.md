

## URLS.



## Installation (via docker)

Install docker and docker compose (on ubuntu 1804)

https://docs.docker.com/install/linux/docker-ce/ubuntu/

https://docs.docker.com/compose/install/


### Increase memory descriptors on system

sudo nano /etc/sysctl.conf
vm.max_map_count=262144
(reboot)
(to avoid rebooting, run:)
sudo sysctl -w vm.max_map_count=262144


### Before Starting for the first time

Make sure to edit the DNS (ip address and or hostname) on the certificates (inside docker compose) to coincide with the IP on the instance
where you want to run the stack

### To run the stack (it may already be running)

from sourcecode/leadzen-es
```
docker-compose up -d
```
To check it is Running
```
docker-compose ps
```

### Bootstrap kibana password

#### NOTE it seems that special characters do not work in the password!

curl -X POST "https://elastic:es-78-training@localhost:9200/_security/user/kibana_system/_password?pretty" --insecure -H 'Content-Type: application/json' -d'
{
  "password" : "dirRytuexugUH7568"
}
'



ssh -i /home/jmm/secret/framework/framework_matt.pem ubuntu@35.178.140.116



