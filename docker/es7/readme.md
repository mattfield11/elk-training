## ELK Stack Training

(this is a readme for a git repository)

The repository contains a Docker based install of Elasticsearch and Kibana  

Follow the instructions below to install Docker and run Elasticsearch and Kibana.

The documentation and excercises for the course are in the folder DOCS.  
It is probably easiest to read the DOCS from the BitBucket web site rather than with a file editor, because of the markdown formatting.

This docker file is designed for install as V.7 and upgrade to V.8.

This avoids the complexity of switching from password based management system to service tokens.

### ES AT SCALE Course

docker should already be installed on bastion host.
all commands work with docker compose (not docker-compose)
do not use sudo with docker compose


#### Increase memory descriptors on system

```
sudo nano /etc/sysctl.conf
vm.max_map_count=262144
```
(reboot)
(to avoid rebooting, run:)
```
sudo sysctl -w vm.max_map_count=262144
```



### Local installation

The following instructions are required to install your own elk stack using docker.


### Hardware Requirements


* At least 4GB Ram
* 30GB or more hard disk (ideally ssd)


### Initial Installation Instructions



#### Install Instructions for Windows 10

Install Git
https://git-scm.com/download/win


Install Docker Desktop (includes Docker compose)

https://docs.docker.com/docker-for-windows/install/

You will need to share hard drive with Docker.
You will have to respond "yes" to prompt:
"Docker wants access to share drive"


#### Install Instructions For Ubuntu 2204 users



Install docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/

Install docker compose

https://docs.docker.com/compose/install/

Increase memory descriptors on system

```
sudo nano /etc/sysctl.conf
vm.max_map_count=262144
```
(reboot)
(to avoid rebooting, run:)
```
sudo sysctl -w vm.max_map_count=262144
```



#### Install and Run the Docker Compose file (For Windows and Ubuntu)

Git clone code from this repo (the repo is public)
```
git clone https://mattfield11@bitbucket.org/mattfield11/elk-training.git
```
```
cd elk-training/docker/es7
```
from here you should see docker-compose.yml 


#### Create Directories on Host Machine

The following directories should be writeable by the user running docker (eg. your user)
Create the directories on the host machine.
```
sudo mkdir /mnt
sudo mkdir /mnt/elasticsearch
sudo mkdir /mnt/elasticsearch/certs
sudo mkdir /mnt/elasticsearch/data01
sudo chown ubuntu:root -R /mnt/elasticsearch

```

#### Set environment Variables for Docker

cp env-example .env



#### Adding SSL Certificates

The certificates in this repository should NOT be used in production, and do not contain the hostnames/IPs of your machines.

If you want to setup your own certificates, then you will need to carry out the following steps

* delete the contents of the certs folder.

* edit instances.yml to include the IP and Hostname (DNS) of your instance.

RUN 
```
docker compose -f create-certs.yml run --rm create_certs
```


#### Run Elasticsearch

Make sure you have followed all the steps above before running the containers.


```
docker compose up -d
```
See logs
```
docker compose logs --tail=500 es01
```
Restart
```
docker compose restart <service_
```
Stop
```
docker compose down
```

#### Test URL

##### On local machines (with no firewall)
Kibana URL is as follows
http://127.0.0.1:5601

Elasticsearch URL via Curl
```
curl -X GET "127.0.0.1:9200/_cat/health?v&pretty"
```


For more information see
https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html



#### Upgrading

This docker compose file is compatible for version 7.16 and version 8 with the following exception:

However version 8 will not allow you to use kibana with the elastic user.  For this reason it is necessary to:

1. Modify the environment variables in .env file
2. Set KIBANA_USER to "kibana_system"
3. Set KIBANA_PASSWORD to "my_kibana_password"
4. Reset the kibana system password to be the same password as you use in step 3.

These steps should be carried out and tested BEFORE upgrading the version.
```
docker compose down
docker compose up -d
```

Once you have tested that you can access kibana with the new changes, then you can upgrade to version 8.












