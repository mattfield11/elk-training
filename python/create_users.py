
'''This script sets up users and spaces in kibana for training.  Set up the users and roles.  
The original version sets up a space and gives users all permissions there index permissions are restricted to prevent accidental deletion'''


import requests
from requests.auth import HTTPBasicAuth

ES_CLUSTER='https://framework.es.europe-west4.gcp.elastic-cloud.com'
KIBANA_URL='https://framework.kb.europe-west4.gcp.elastic-cloud.com'
ES_PASSWORD='6aGnopenbPJtiOsF504EfoMO'
ES_USER='elastic'




all_users=[
  {"username":"matt","initials":"mf","password":"es-78-training","roles":["superuser"]},
  {"username":"framework","initials":"fw","password":"es-78-training","roles":["training_role"]},
   {"username":"rick_hermans","initials":"rh","password":"es-78-training","roles":["training_role"]},
    {"username":"roger_mooren","initials":"rm","password":"es-78-training","roles":["training_role"]},
     {"username":"maurice_sleijpen","initials":"ms","password":"es-78-training","roles":["training_role"]},
      {"username":"jonnik_smeets","initials":"js","password":"es-78-training","roles":["training_role"]},
       {"username":"marco_vanderwoude","initials":"mv","password":"es-78-training","roles":["training_role"]},
        {"username":"david_vankan","initials":"dv","password":"es-78-training","roles":["training_role"]},
        {"username":"user1","initials":"u1","password":"es-78-training","roles":["training_role"]},
        {"username":"rob_keulen","initials":"rk","password":"es-78-training","roles":["training_role"]},
        {"username":"maarten_esch","initials":"me","password":"es-78-training","roles":["training_role"]}

]


#POST /_security/role/my_admin_role
def create_training_role():
  return {
  "cluster": ["all"],
  "indices": [
    {
      "names": [ "framework*" ],
      "privileges": ["all"]
    },
     {
      "names": [ "*" ],
      "privileges": ["read","view_index_metadata"]
    }
  ],

  "metadata" : {
    "version" : 1
  }
}

#POST /_security/user/jacknich

def create_user(user,password,roles):
  return {
  "password" : password,
  "roles" : roles,
  "full_name" : user,
 
}





#$ curl -X POST api/spaces/space
def create_space_json(user,initials):
  return {
  "id": user,
  "name": user,
  "description" : "User training space",
  "initials": initials,
  "disabledFeatures": []

}


#$ curl -X PUT api/security/role/my_kibana_role
def add_access_space_json(user):
  #provides access to own space
  return {
  "metadata" : {
    "version" : 1
  },
  "elasticsearch": {
    "cluster" : [ ],
    "indices" : [ ]
  },
  "kibana": [
    {
      "base": ["all"],
      "feature": {
      },
      "spaces": [
        user
      ]
    }
  ]
}



def main():
  role_name="training_role"
  http_url= ES_CLUSTER+"/_security/role/"+role_name
  r = requests.post(http_url, json=create_training_role(),auth=HTTPBasicAuth(ES_USER, ES_PASSWORD))
  print (f"returns {r.status_code}")
  print (f"response {r.json()}")

  for user in all_users:
    kibana_headers = {'content-type': 'application/json','kbn-xsrf': 'true'}
    space_role="spacerole-"+user['initials']
    http_url= KIBANA_URL+"/api/security/role/"+space_role
    r = requests.put(http_url, headers=kibana_headers,json=add_access_space_json(user['username']),auth=HTTPBasicAuth(ES_USER, ES_PASSWORD))
    print (f"creating {space_role} role for user {user['username']}")
    print (f"returns {r.status_code}")
    #print (f"response {r.json()}")
    print (f"adding {space_role} to list of user roles")
    user['roles'].append(space_role)
    
    http_url= ES_CLUSTER+"/_security/user/"+user['username']
    r = requests.post(http_url, json=create_user(user['username'],user['password'],user['roles']),auth=HTTPBasicAuth(ES_USER, ES_PASSWORD))
    print (f"creating user {user['username']}")
    print (f"returns {r.status_code}")
    print (f"response {r.json()}")
    
   
    http_url= KIBANA_URL+"/api/spaces/space"
    r = requests.post(http_url, headers=kibana_headers, json=create_space_json(user['username'],user['initials']),auth=HTTPBasicAuth(ES_USER, ES_PASSWORD))
    print (f"creating space for user {user['username']}")
    print (f"returns {r.status_code}")
    print (f"response {r.json()}")

  for user in all_users:
    print (f"user {user['username']}")
    print (f"password {user['password']}")
    print (f"url {KIBANA_URL}")
    print (" ")






main()

