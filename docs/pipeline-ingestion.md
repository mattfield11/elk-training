
## Simulate the pipeline

(Opensearch Compatible)


```
POST /_ingest/pipeline/_simulate?verbose&pretty
{
  "pipeline": {
    "description": "Add user/job fields",
    "processors": [
      {
        "set": {
          "field": "user",
          "value": "john"
        }
      },
      {
        "set": {
          "field": "job",
          "value": 10
        }
      }
    ],
    "version": 1
  },
  "docs": [
    {
      "_index": "index",
      "_id": "1",
      "_source": {
        "name": "docs1"
      }
    },
    {
      "_index": "index",
      "_id": "2",
      "_source": {
        "name": "docs2"
      }
    }
  ]
}

```

## Built-in processors

```
POST /_ingest/pipeline/_simulate?pretty
{
  "pipeline": {
    "description": "Testing some build-processors",
    "processors": [
      {
        "dot_expander": {
          "field": "extfield.innerfield"
        }
      },
      {
        "remove": {
          "field": "unwanted"
        }
      },
      {
        "trim": {
          "field": "message"
        }
      },
      {
        "set": {
          "field": "tokens",
          "value": "{{message}}"
        }
      },
      {
        "split": {
          "field": "tokens",
          "separator": "\\s+"
        }
      },
      {
        "sort": {
          "field": "tokens",
          "order": "desc"
        }
      },
      {
        "convert": {
          "field": "mynumber_text",
          "target_field": "mynumber_int",
          "type": "integer"
        }
      }
    ]
  },
  "docs": [
    {
      "_index": "index",
      "_id": "1",
      "_source": {
        "extfield.innerfield": "booo",
        "unwanted": 32243,
        "message": "   155.2.124.3 GET /index.html 15442 0.038   ",
        "mynumber_text": "3123"
      }
    }
  ]
}
```

## Grok processor

In this example we define a custom grok definition.
Usually this is not necessary because there are many definitions already available.

Of the two documents below, the first one passes, the second throws an error

```
POST /_ingest/pipeline/_simulate?pretty
{
  "pipeline": {
  "description" : "custom grok pattern",
  "processors": [
    {
      "grok": {
        "field": "message",
        "patterns": ["my favorite color is %{COLOR:color}"],
        "pattern_definitions" : {
          "COLOR" : "RED|GREEN|BLUE"
        }
      }
    }
  ]
},
"docs":[
  {
    "_source": {
      "message": "my favorite color is RED"
    }
  },
  {
    "_source": {
      "message": "this document will cause the pipeline to fail!!"
    }
  }
  ]
}
```




## Simulate pipeline

```
# Start from docs, fill in processors later
POST _ingest/pipeline/_simulate
{
 "pipeline": {},
 "docs": [
   {
     "station": "BMT,4 Avenue,59th St,40.641362,-74.017881,N,R,,,,,,,,,,Stair,YES,,YES,NONE,,FALSE,,TRUE,4th Ave,60th St,SW,40.640682,-74.018857,'(40.641362, -74.017881)','(40.640682, -74.018857)'"
   }
 ]
}
```

Simulate the grok processor:

```
POST _ingest/pipeline/_simulate
{
 "pipeline": {
   "description": "Parsing the NYC stations",
   "processors": [
     {
       "grok": {
         "field": "station",
         "patterns": [
           "%{WORD:division},%{DATA:line},%{DATA:station_name},%{NUMBER:location.lat},%{NUMBER:location.lon},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA},%{DATA:entrance_type},%{DATA:entry},%{DATA:exit_only},%{DATA:vending}"
         ]
       }
     },
     {
       "remove": {
         "field": "station"
       }
     }
   ]
 },
 "docs": [
   {
     "_index": "subway_info",
     "_id": "AVvJZVQEBr2flFKzrrkr",
     "_score": 1,
     "_source": {
       "station": "BMT,4 Avenue,53rd St,40.645069,-74.014034,R,,,,,,,,,,,Stair,NO,Yes,NO,NONE,,FALSE,,TRUE,4th Ave,52nd St,NW,40.645619,-74.013688,'(40.645069, -74.014034)','(40.645619, -74.013688)'"
     }
   }
 ]
}
```

