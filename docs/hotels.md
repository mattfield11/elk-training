
### Hotels MD Excercise

Introduction to indexing and mapping ES Version 7

## Create Index
```
PUT framework-hotels
```

## Create index with shards and replicas

```
DELETE framework-hotels

PUT framework-hotels
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3,
            "number_of_replicas" : 2
        }
    }
}
'
```

## Without curl, using Kibana

```
PUT framework-hotels?pretty
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3,
            "number_of_replicas" : 2
        }
    }
}
```

## List indexes
```
GET /_cat/indices?v
```

## Import sample data

```
POST framework-hotels/_doc/1
{
    "type" : "hotel",
    "name" : "Motif Seattle",
    "city" : "Seattle",
    "countryCode" : "US",
    "hotelRating" : 4,
    "location" : {
      "lat" : 47.60985,
      "lon" : -122.33475
    }
}
```
```
GET framework-hotels/_doc/1
```

## Update the existing record

```
PUT framework-hotels/_doc/1
{
    "type" : "hotel",
    "name" : "Motif Seattle",
    "city" : "Seattle",
    "countryCode" : "US",
    "hotelRating" : 4,
    "location" : {
      "lat" : 47.60985,
      "lon" : -122.33475
    }
}
```


## Search all data
```
POST framework-hotels/_search
```

## Simple search
```
GET /framework-hotels/_search
{
    "query": {
        "match" : {
           "name" : "Motif"
        }
    }
}
```

## Partial update
```
POST /framework-hotels/_update/1
{
   "doc" : {
       "name" : "Motif",
       "hotelRating": 3
   }
}
```

## Scripted partial update
```
POST /framework-hotels/_update/1
{
   "script" : "ctx._source.hotelRating += 1"
}
```

## Upsert

Run the following 2 queries twice.
Notice that the document changes. The first time, no document exists,
so the document is UPSERTED.  The second time the document exists,
so the script modifies the document.

```
POST /framework-hotels/_update/2
{
    "script" : {
        "source": "ctx._source.hotelRating = params.rating",
        "lang": "painless",
        "params" : {
            "rating" : 4
        }
    },
    "upsert" : {
        "hotelRating" : 1
    }
}
```
```
GET framework-hotels/_doc/2
```

## Deleting documents
```
DELETE /framework-hotels/_doc/2
```

## Delete by query
```
POST framework-hotels/_delete_by_query
{
  "query": {
    "match": {
      "name": "Motif"
    }
  }
}
```

