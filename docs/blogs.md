
## Blog examples

## Define the mapping and import the data

Bulk import the data
```
curl -H "Content-Type: application/json" -XPOST "http://127.0.0.1:9200/blog/_bulk?pretty" --data-binary "@blog.json"
```

## Check the mapping
```
GET blogs/_mapping
```


## Consider the following query
```
POST blog/_search
{
  "query":{
    "match":{
      "content.rendered":{
        "query":"ux designs"
      }
    }
  }
}
```

How can we improve the results?

* Try adding a highlighter
* Consider improving both the query and the analyzer
* Create a template for index blogs-test 
* Reindex the data and test your improvements
* Iterate!






## Bool query with filtering
```
GET /_search
{
  "query": {
    "bool": {
      "must": [
        { "match": { "title.rendered": "Analytics" }}
      ],
      "filter": [
        { "term":  { "status": "publish" }},
        { "range": { "date_gmt": { "gte": "2017-11-01" }}}
      ]
    }
  }
}
```






## References

* Data source: https://www.sitepoint.com/wordpress-json-example/
