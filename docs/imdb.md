# Section 3 - Mapping IMDB movies analysis

## Import the data

```
curl -H "Content-Type: application/json" -XPOST "http://127.0.0.1:9200/imdb/_bulk?pretty" --data-binary "@imdb.json"
```

##�Check mapping
```
GET imdb/_mapping
```

## Task: Add custom mapping for IMDB
* Review each field and make a decision on what should be changed and why


## Stop Here

# Section 5 - Analyzers

```
POST _analyze
{
  "analyzer": "standard",
  "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
}

POST _analyze
{
  "tokenizer" : "keyword",
  "filter" : ["lowercase"],
  "char_filter" : ["html_strip"],
  "text" : "this is a <b>test</b>"
}
```


### A synonym analyzer

#### Create a test index with a synonym analyzer

```
PUT /test_index
{
  "settings": {
    "index": {
      "analysis": {
        "analyzer": {
          "synonym": {
            "tokenizer": "standard",
            "filter": [
             
              "synonym",
               "lowercase",
              "my_stop"
            ]
          }
        },
        "filter": {
          "my_stop": {
            "type": "stop",
            "stopwords": [
              "bar"
            ]
          },
          "synonym": {
            "type": "synonym",
            "lenient": true,
            "synonyms": [
              "new york => nyc"
            ]
          }
        }
      }
    }
  },
  "mappings": {
    "properties":{
    "geoip.city_name": {
      "type": "text",
      "fields": {
        "synonym": {
          "type": "text",
          "analyzer": "synonym"
        }
      }
    }
  }
}
}
```






#### Copy the original data into your test index in order to test

```
POST _reindex
{
  "source": {
    "index": "kibana_sample_data_ecommerce"
  },
  "dest": {
    "index": "test_index"
  }
}
```

#### Attention!
The above command produces an error!! 
Do we need to fix it, and if so how?



```
POST test_index/_analyze
{
  "field":"geoip.city_name.synonym",
  "text":"new york"
}
```
#### Test the analyzer with a query

The following should work but doesn't - why ???
```
POST test_index/_search
{
  "query":{
    "match":{
      "geoip.city_name.synonym":{
        "query":"new york"
      }
    }
  }
}

```

#### Questions

* What happens if i search for just "new york" why?  (think Upper/Lower case.)

* How can we solve this?






### Use of a Normalizer
```
PUT test/_mapping
{
  "dynamic": "false",
  "properties": {
    "new_field": {
      "type": "keyword",
      "fields": {
        "numeric": {
          "type": "integer"
        },
        "text": {
          "type": "text",
          "analyzer": "my_lowercase"
        }
      }
    },
    "log_level":{
      "type":"keyword",
      "normalizer":"lowercase"
    }
  }
}
```
Check the normalizer works
```
POST test/_analyze
{
  "field": "log_level",
  "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
}
```


## Stop Here

## Section 6 Search

```
POST imdb/_search
{
  "_source": "originalTitle",
  "explain": true,
  "query": {
    "match": {
      "actors": "Leonardo DiCaprio"
    }
  }
}

# see _explanation
```

## Term query
```
GET imdb/_search
{
  "query": {
    "term" : { "actors.keyword" : "Uma Thurman" }
  }
}
```
## Question, why this doesn't work?

```
GET imdb/_search
{
  "query": {
    "term" : { "actors" : "Uma Thurman" }
  }
}
```
```
GET /imdb/_search
{
    "query": {
        "constant_score" : {
            "filter" : {
                "terms" : { "actors.keyword" : ["Uma Thurman", "John Travolta"]}
            }
        }
    }
}
```

## Range queries
```
GET /imdb/_search
{
    "query": {
        "range" : {
            "imdbRating" : {
                "gte" : 9
           }
        }
    }
}
```

## Date range queries

```
GET /imdb/_search
{
    "query": {
        "range" : {
            "year" : {
                "gte" : "2016",
                "lt" :  "now/d",
                "format": "yyyy"

            }
        }
    }
}
```
## Exists

```
GET /imdb/_search
{
    "query": {
        "exists" : { "field" : "ratings" }
    }
}
```

## Wildcard queries

```
GET /imdb/_search
{
    "query": {
        "wildcard" : { "actors.keyword" : "T* Hanks" }
    }
}

```

## Tasks:

* Create a query to find films boost title and originalTitle * 3

* Which hotels have Health club , and more than 3 stars?

```
POST imdb_v5/_search
{
  "query": {
    "bool" : {
      "must" : [],
      "filter":[] ,
      "must_not" : [],
      "should" : [{
           "multi_match" : {
      "query":    "redemption",
      "fields": [ "title^3", "originalTitle" ]
    }}

      ],
      "minimum_should_match" : 1,
      "boost" : 1.0
    }
  }
}

```
```
POST hotels/_search
{
  "query": {
    "bool" : {
      "must" : [{"term":{"service.keyword":"Health Club"}},{
        "range" : {
            "stars" : {
                "gte" : 4

            }
        }
    }],
      "filter":[] ,
      "must_not" : [],
      "should" : [

      ],
      "minimum_should_match" : 0,
      "boost" : 1.0
    }
  }
}
```
## More like this query

### Using text as a template

```
GET /imdb/_search
{
    "_source": "originalTitle",
    "query": {
        "more_like_this" : {
            "fields" : ["originalTitle"],
            "like" : ["The Godfather"],
            "min_term_freq" : 1,
            "min_doc_freq": 1,
            "minimum_should_match": "10%"
        }
    }
}
```

### Using existing document as a tempalte
(change ID as appropriate)
```
GET /imdb/_search
{
    "_source": "title",
    "query": {
        "more_like_this" : {
            "fields" : ["title"],
            "like" : [
              {
                  "_index" : "imdb",
                  "_type" : "default",
                  "_id" : "AWDWO6XOiJp4Dnk9LZmq"
              }
            ],
            "min_term_freq" : 1,
            "min_doc_freq": 1,
            "minimum_should_match": "10%"
        }
    }
}
```


### see for more info:
https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-term.html





## Stop Here

## Aggregations

Calculate average rating across all movies
```
POST /imdb/_search?size=0
{
    "aggs" : {
        "avg_rating" : { "avg" : { "field" : "ratings" } }
    }
}
```

Rating statistics
```
POST /imdb/_search?size=0
{
    "aggs" : {
        "stats_rating" : { "stats" : { "field" : "ratings" } }
    }
}
```

Movie genres
```
GET /imdb/_search
{
    "size" : 0,
    "aggs" : {
        "genres" : {
            "terms" : {
              "field" : "genres.keyword",
              "missing" : "N/A",
              "min_doc_count" : 0,
              "size" : 5
            }
        }
    }
}
```

## Subaggregation
```
GET /imdb/_search
{
  "size": 0,
  "query": {
    "range": {
      "year": {
        "gte": 2000
      }
    }
  },
  "aggs": {
    "genres_terms": {
      "terms": { "field": "genres.keyword" },
      "aggs": {
        "genres_stats": {
          "stats": { "field": "ratings" }
        }
      }
    }
  }
}
```

Notes:
* `doc_count_error_upper_bound`: an upper bound of the error on the document counts for each term
* `sum_other_doc_count`: when there are lots of unique terms, elasticsearch only returns the top terms; this number is the sum of the document counts for all buckets that are not part of the response

## Filter aggregation

See: https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-filter-aggregation.html
```
GET /imdb/_search
{
  "size": 0,
  "aggs": {
    "low_value": {
      "filter": {
        "range": {
          "imdbRating": {
            "gte": 6
          }
        }
      }
    }
  }
}
```

## Task

* How many movies there were before 1990, 1990-2000, 2000+
See: https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-daterange-aggregation.html


* Create a histogram of movies by their rating (1-2, 2-3, ...)
See: https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-histogram-aggregation.html

* Create a date-histogram of movies by their dates (1 years buckets)
See:
* https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-datehistogram-aggregation.html
* https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#time-units


#### Bonus track

Find the average rating for the films in the periods 1990, 1990-2000, 2000+

https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-metrics-avg-aggregation.html


#### Answers

```
POST /imdb/_search?size=0
{
    "aggs": {
        "range": {
            "date_range": {
                "field": "releaseDate",
                "format": "yyyy-MM-dd",
                "ranges": [
                   {"from":"now-30y/M", "to": "now-20y/M" },
                    {"from":"now-20y/M", "to": "now-10y/M" },
                    { "from": "now-10y/M" }
                ]
            }
        }
    }
}
```
```
POST /imdb/_search?size=0
{
    "aggs" : {
        "by_rating" : {
            "histogram" : {
                "field" : "ratings",
                "interval" : 1
            }
        }
    }
}
```

NOTE That calendar_interval can only be single intervals, multiples eg(5y) are not accepted
```
POST /imdb/_search?size=0
{
    "aggs" : {
        "sales_over_time" : {
            "date_histogram" : {
                "field" : "releaseDate",
                "calendar_interval" : "1y"
            }
        }
    }
}
```
For specific periods, you can use date range aggregation
```
POST /imdb/_search?size=0
{
    "aggs": {
        "range": {
            "date_range": {
                "field": "releaseDate",
                "format": "yyyy-MM-dd",
                "ranges": [
                   {"from":"now-30y/M", "to": "now-20y/M" },
                    {"from":"now-20y/M", "to": "now-10y/M" },
                    { "from": "now-10y/M" }
                ]
            },

         "aggs" : {
        "avg_rating" : { "avg" : { "field" : "ratings" } }
    }
    }}
}
```
