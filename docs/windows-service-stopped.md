## Execercise - Service Stopped.


We are interested in creating an alarm when windows services stop.

* What fields do we have that give us information about windows services that can help us? (clue: look in metricbeat)

* Can you think of two different approaches we can use to create this alarm?

* We want to see this alarm when a specific server stops on a specific server

* In the output we want to see the name of the server, the service that stopped

* We want to be reminded the service is stopped every 4 hours.

* Try to create this alarm twice, using both of these possible approaches.

* How can we test the alarm without stopping the service?

* Would your alarm work if there was no data?

* How do we change the alarm if we only want an alert if a service is down for more than 30 minutes?





Server name - GBLDNSRV02405

Service name - ZE Data Manager

Status we are alerting on – We need to get alert, when the service gets stopped.

 

Afternoon Session

Server name - gbldnsrv4pw4321

Service name – WCF Host

Status we are alerting on – We need to get alert, when the service gets stopped.

 

