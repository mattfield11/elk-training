
## Pageing Excercise

1. Adapt the following commands to test the various ways of pageing to one of the data sets on the cluster
2. Verify that the commands work as you expect
3. What implications does the above have for web design of pageing?

Think about - 

* We want to provide buttons so that users can page through results
* We want to allow a google bot to visit every page on our site

## Simple Pageing


```
GET /_search
{
  "from": 5,
  "size": 20,
  "query": {
    "match": {
      "user.id": "kimchy"
    }
  }
}
```


## Search After Initial Request

```
GET twitter/_search
{
    "query": {
        "match": {
            "title": "elasticsearch"
        }
    },
    "sort": [
        {"date": "asc"},
        {"tie_breaker_id": "asc"}      
    ]
}

```


## Search After Second Request

```
GET twitter/_search
{
    "query": {
        "match": {
            "title": "elasticsearch"
        }
    },
    "search_after": [1463538857, "654323"],
    "sort": [
        {"date": "asc"},
        {"tie_breaker_id": "asc"}
    ]
}

```

## Scroll - Suitable For API requests (depends on second request being passed within limited timeframe)

```

POST /my-index-000001/_search?scroll=1m
{
  "size": 100,
  "query": {
    "match": {
      "message": "foo"
    }
  }
}

```

```

POST /_search/scroll                                                               
{
  "scroll" : "1m",                                                                 
  "scroll_id" : "DXF1ZXJ5QW5kRmV0Y2gBAAAAAAAAAD4WYm9laVYtZndUQlNsdDcwakFMNjU1QQ==" 
}

```



