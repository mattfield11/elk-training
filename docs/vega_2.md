## Vega Lite


Vega Lite is a powerful visualisation language that allows you to define your own aggregations, transformations and how they are displayed.

The below is an example

* You will need to go to the HOME menu to load the sample data set.

* Go to Dashboard > Create > All Types > Custom Visualisation.

* You can see that the "body" defines an elasticsearch aggregation.

* The query is built using the %context% which uses the time picker to filter the results

* The format property defines where to take the results from

* In this case a transform is required to flatten the time buckets into a series of points.

* The encoding indicates where to take the values for the data points.

Main applications of Vega are where you may want to use aggregations that are not available in standard kibana widgets.
eg. nested aggregations.

The disadvantage is the technical difficulty of constructing the widgets and limited means of debugging.


```

{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "title": "newEvent counts from all indexes",
  "data": {
    "url": {
      "%context%": true,
      "%timefield%": "order_date",
      "index": "kibana_sample_data_ecommerce",
      "body": {
        "aggs": {
          "categories": {
            "terms": {"field": "category.keyword"},
            "aggs": {
              "time_buckets": {
                "date_histogram": {
                  "field": "order_date",
                  "interval": {%autointerval%: true},
                  "extended_bounds": {
                    "min": {%timefilter%: "min"},
                    "max": {%timefilter%: "max"}
                    },
                  "min_doc_count": 0
                }
              }
            }
          }
        },
        "size": 0
      }
    },
    "format": {"property": "aggregations.categories.buckets"}
  },
  "transform": [{"flatten": ["time_buckets.buckets"], "as": ["buckets"]}],
  "mark": "area",
  "encoding": {
    "x": {"field": "buckets.key", "type": "temporal", "axis": {"title": null}},
    "y": {
      "field": "buckets.doc_count",
      "type": "quantitative",
      "axis": {"title": "Document count"}
    },
    "color": {"field": "key", "type": "nominal"}
  }
}

```



### The following is a link with a good gallery and coding examples 


https://vega.github.io/vega-lite/examples/