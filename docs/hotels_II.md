

## Check cluster health
```
GET _cluster/health
```

## Cluster commands
```
# Displays all commands
GET /_cat

# Health
GET /_cat/health
# Use format=json if preferred
GET /_cat/health?format=json
# Each command accepts help
GET /_cat/health?help

# Display results with column headers (verbose)
GET /_cat/nodes?v

# List indices
GET /_cat/indices?v

# Shards allocation
GET /_cat/allocation?v
```




### Check existing mapping and compare it with the index already on the cluster hotels

```
GET hotels/_mapping
```

### Delete and recreate the mapping
```
DELETE /hotels-test
```


```
PUT hotels-test/
{
  "mappings" : {
     "properties" : {
        "name": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
        "stars" : { "type" : "byte" },
        "rooms" : { "type" : "short" },
        "location" : { "type" : "geo_point" },
        "city" : { "type" : "keyword" },
        "address": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
        "internet" : { "type" : "boolean" },
        "service" : { "type" : "keyword" },
        "checkin": { "type" : "date", "format" : "dateOptionalTime"}
      }
    }
  }
```



## Task - Geo query

Find all hotels in 2 km radius from the
point: 37.556035, 127.005232
(latitude, longitude)


https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-geo-distance-query.html

### Bonus question

Modify the query to return only those hotels with internet


## Sorting and pagination

```
GET /hotels/_search
{
    "sort" : [
       { "price" : {"order" : "asc" } },
        "_score"
    ],
    "query" : {
        "term" : { "service" : "spa" }
    }
}
```

# Aggregations

## Filters aggregation

See: https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-filters-aggregation.html
```
GET hotels/_search
{
  "size": 0,
  "aggs" : {
    "messages" : {
      "filters" : {
        "filters" : {
          "spas" :   { "match" : { "service" : "spa"   }},
          "weddings" : { "match" : { "service" : "wedding" }}
        }
      }
    }
  }
}
```

## Range aggregation

See: https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-range-aggregation.html

```
GET hotels/_search
{
  "size": 0,
  "aggs" : {
        "price_ranges" : {
            "range" : {
                "field" : "price",
                "ranges" : [
                    { "to" : 150 },
                    { "from" : 150, "to" : 200 },
                    { "from" : 250, "to" : 300 },
                    { "from" : 300 }
                ]
            }
        }
    }

}
```


References:
* Data source: https://github.com/wikibook/elasticsearch
