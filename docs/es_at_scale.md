## Set up and Maintenance of Elasticsearch

## DAY 1

### Creating an elastic cluster from scratch 

* Presentation 010 Introduction to Elk Stack  (30)


### Architecture 


* Presentation 020 Architecture   (30)
* Presentation 030 Configuration  (30)


### Setup Cluster

* Setup Version 7.17 Cluster (Ubuntu ([systemd install](1-20_installation_systemd.MD)))   (120)

### Setup A Monitoring Cluster (With Docker)  (90)

* [Practical](../docker/es7/readme.md)
* [Setting up Monitoring with Metricbeat](./monitoring_with_metricbeat.MD)  
* Walk through of monitoring panel in kibana 





### Snapshot and Recovery   (60)


* Presentation 080 Snapshot and Recovery 
* Practical - [Install S3 on cluster](./snapshot_restore.MD)
* Practical - Create snapshot policy
* Practical - Recover snapshot
* Practical - Install on docker cluster and recover index there.


## Day 2

### Log Ingestion 

#### Logstash (120)

* Presentation 06 - Logstash
* Practical - [Install logstash using docker](https://bitbucket.org/mattfield11/log-generator/src/master/)
* Practical - logstash pipeline (log-generator)
* Practical - [Monitoring logstash with metricbeat](./monitoring_with_metricbeat_logstash.MD) - sending monitoring data from logstash to elasticsearch 



### Elasticsearch At Scale  


#### ILM (Index lifecycle Management)   (60)


* [ILM](./ilm_introduction.MD)




#### Performance and Mapping Optimization   (60)

* Presentation 050 Speed and Responsiveness
* Detect Ingest Delays (Log Generator)
* [Performance](./cluster_performance.MD)

* Mapping 
    - Presentation 03 - Mapping

REVISE Presentation

* Practical - apply dynamic mapping rules to generator logs.
* Practical - solve mapping conflicts when changing mapping types (following upgrade)

### Architecture (60)

* Presentation 020 Architecture (revisited)
* [Practical Hot/Cold and ZoneAwareness](./zone_awareness_hot_cold.MD)





#### Data Streams   (30)

* Data Management 
* [Data rollup](./data_rollups.MD) 





## Day 3 


### Upgrade Process   (90)


* [Practical - Upgrade Process](./upgrading_elasticsearch.MD) 
* [Upgrade logstash client](https://bitbucket.org/mattfield11/log-generator/src/master/) 




### WalkThrough ElasticCloud / AWS (30)


Setup on ElasticCloud
Setup on AWS (managed service)


 


### Kibana


#### Data discovery  (60)

* Exploring data
* Finding fields
* Filters (pinning, inverting, editing)
* Saving queries
* Exporting queries
* Introduction to KQL
    - wildcard
    - AND/OR
* Introduction to Elasticsearch Queries 
    - term
    - range
    - match
    - bool query

* Practical Excercise
    - [queries questions](./advanced_queries.MD)





#### Dashboards,tables, graphs  (60)

* Tables
* Graphs
* Logs list
* Drilldowns


#### Practical Excercise    (30)
    * Setup Dashboard for Logs





### Alerts  (30)

* Setup Alerts for Elasticsearch Cluster 

see [alerts-excercise](./alerts-questions.MD)

* Setup Alarms for Metrics
* Setup Alarms based on Elasticsearch Query



### Security  (30)

* Kibana Workspaces
* Security



#### Introduction to Canvas and Vega (30)

[introduction](./vega.md)




#### Ingestion Pipelines    (60)

* [ingest pipelines](pipeline-ingestion.MD)
* Practical - Create Ingestion Pipeline for Log-generator logs 



