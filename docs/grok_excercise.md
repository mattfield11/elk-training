## Grok

Grok is one of the main tools used for log parseing.

Grok expressions are based on regular expressions, and allow you to parse logs into fields.

You can test GROK expressions on the KIBANA development panel.

Select Management> Dev Tools on the left hand menu.

The select the tab Grok Debugger.

Try to Use GROK with the following Log Line.
```
Feb 28 11:25:59 lgrc-fw-106-network 1 2019-02-28T16:25:59.118Z lgrc-fw-106-7 RT_FLOW - RT_FLOW_SESSION_DENY [junos@2636.1.1.1.2.34 source-address="59.23.77.51" source-port="123" destination-address="128.119.118.29" destination-port="123" service-name="junos-ntp" protocol-id="17" icmp-type="0" policy-name="inbound-drop" source-zone-name="outside" destination-zone-name="inside" application="UNKNOWN" nested-application="UNKNOWN" username="N/A" roles="N/A" packet-incoming-interface="reth1.0" encrypted="UNKNOWN" reason="policy deny"]
```

Step 1 Paste the line above into "sample data"

Step 2 Start with the following grok pattern.

\A%{GREEDYDATA:stub_message}

Step 3 Progressively add new grok patterns to break down the log line into fields.

eg. \A%{SYSLOGTIMESTAMP:syslog_timestamp}%{SPACE}%{GREEDYDATA:stub_message}

Choose GROK patterns from the following:
```
\A
%{SYSLOGTIMESTAMP:syslog_timestamp}
%{SPACE}
%{SYSLOGHOST:hostname}
%{NUMBER}
%{TIMESTAMP_ISO8601:timestamp}
%{NOTSPACE}
%{SYSLOGPROG}
%{GREEDYDATA:stub_message}
```

You can also use fixed characters eg. -  :  
Certain characters need escaping, such as \[



Test whether your grok pattern still works when you use a different log line...
```
Feb 28 11:25:59 lgrc-fw-106-network 1 2019-02-28T16:25:59.118Z lgrc-fw-106-7 RT_FLOW - RT_FLOW_SESSION_CREATE [junos@2636.1.1.1.2.34 source-address="128.119.119.204" source-port="63591" destination-address="128.119.101.1" destination-port="53" service-name="junos-dns-udp" nat-source-address="128.119.119.204" nat-source-port="63591" nat-destination-address="128.119.101.1" nat-destination-port="53" src-nat-rule-type="N/A" src-nat-rule-name="N/A" dst-nat-rule-type="N/A" dst-nat-rule-name="N/A" protocol-id="17" policy-name="dns-ntp" source-zone-name="inside" destination-zone-name="outside" session-id-32="200213229" username="N/A" roles="N/A" packet-incoming-interface="reth0.560" application="UNKNOWN" nested-application="UNKNOWN" encrypted="UNKNOWN"]
```



You can also use:

http://grokconstructor.appspot.com

