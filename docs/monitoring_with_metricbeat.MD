## Setup Monitoring With Metricbeat

### On the Production Cluster (ie the one you want to monitor)

Important - do not enable monitoring the docker monitoring cluster because that will setup self monitoring and prevent you being 
able to monitor another cluster!




### Install metricbeat 

We will install metricbeat just on the MONITORING instance.


Be careful about which version you install.  The version should always be equal to the version of ES you are running on your cluster.
Different versions of linux may require different distributions (see metricbeat installation docs for more details)

```
curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.17.6-amd64.deb
sudo dpkg -i metricbeat-7.17.6-amd64.deb
```

### Enable ES module on metricbeat

```
sudo metricbeat modules enable elasticsearch-xpack
```

### Disable collection of system metrics (optional)
```
metricbeat modules disable system
```

### Create a Monitoring User on the Production Cluster.

We need a user with permissions to collect data from elasticsearch production.
We can use the built in remote_monitoring_user with the remote_monitoring_collector role.




### Edit Metricbeat Configuration

All of the following configurations relate to the data COLLECTOR.
ie cluster/user and password of PRODUCTION ELASTICSEARCH.

#### Copy the CA certificate for ES into the metricbeat config directory

We have two CA certs , one for the docker cluster, another for the production cluster

For "es at scale" we have already installed the cert into /certs/ca/ca.crt so we can skip the following.


```
sudo mkdir /certs
sudo mkdir /certs/ca/ca.crt
sudo mkdir /certs/es-mon/
sudo cp /etc/elasticsearch/certs/ca/ca.crt /certs/ca/ca.crt
sudo nano /certs/es-mon/ca.crt
sudo chown metricbeat:root -R /certs
```


#### Edit the configuration.


```
sudo nano /etc/metricbeat/modules.d/elasticsearch-xpack.yml
```

The following refers to the PRODUCTION cluster

```
  - module: elasticsearch
    xpack.enabled: true
    period: 10s
    hosts: ["https://10.0.101.100:9200","https://10.0.102.100:9200","https://10.0.103.100:9200"] 
    scope: cluster
    username: "remote_monitoring_user"
    password: "secret"
    ssl.enabled: true
    ssl.certificate_authorities: ["/certs/ca/ca.crt"]
    #ssl.certificate: "/etc/pki/client/cert.pem"
    #ssl.key: "/etc/pki/client/cert.key"
    #ssl.verification_mode: "full"
```




Note that the config ABOVE refers to PRODUCTION while the config BELOW refers to the monitoring cluster.
It may be necessary to reset the password of the remote_monitoring_user.
Especially be careful with the ssl.certificate_authorities ; the monitoring cluster may or may not have been setup with the same ca.crt
as the production cluster- don't get them confused, especially since they may have the same name!

It may be necessary to copy the ca.crt from the monitoring.


```
sudo nano /etc/metricbeat/metricbeat.yml
```



```

output.elasticsearch:
  # Array of hosts to connect to.
  hosts: ["https://localhost:9200", "http://localhost:9200"] 

  # Optional protocol and basic auth credentials.
  protocol: "https"
  username: "elastic"
  password: "training"
  #for brevity set to "none"... for production set to "full" you will need to install the ca.crt
  ssl.verification_mode: "none"
  ssl.certificate_authorities: ["/certs/es-mon-ca/ca.crt"]
```


## Restart metricbeat
```
sudo systemctl restart metricbeat
```
### Check the metricbeat logs

```
sudo tail -n 1000 /var/log/metricbeat/metricbeat.log
```

### Enabling monitoring (only on production cluster)
Normally not necessary.

```
PUT _cluster/settings
{
  "persistent": {
    "xpack.monitoring.collection.enabled": true
  }
}
```

### Upgrading

Rules for upgrading metricbeat

* In general, metricbeat should ALWAYS be kept as same version as all other products in the stack. 
* When upgrading, the order should be MONITORING CLUSTER,METRICBEAT, PRODUCTION CLUSTER. (ie monitoring should never be lower version than cluster to be monitored)
* Just like ES, when upgrading major versions to avoid problems you should always use the LAST major version as a stepping stone eg.7.4->7.17->8.3
* Beware of changes in configuration.  Although you may be able to cut and paste, it is best to backup your old config, install with blank config, and then use the editor to restore any changes.

* For metricbeat the main change is that instead of using indices metricbeat starts to use DATASTREAMS in v.8. (make sure that the templates for each version are loaded.)










### Reference


https://www.elastic.co/guide/en/elasticsearch/reference/current/monitoring-production.html


