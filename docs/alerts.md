## Alerts in Kibana

### Example Document to Index 





{ 
    "doc_type":"alert",
    "user_name":"matt", 
    "@timestamp":"{{date}}",
    "top_hit":"{{context.hits}}", 
    "context_conditions":"{{context.conditions}}",
    "context_message":"{{context.message}}",
    "context_title":"{{context.title}}",
    "context_value":"{{context.value}}",
    "rule_name":"{{rule.name}}" }



### Trouble shooting

Status:OK means alarm is running, but not alerting.
Status: active means alarm is alerting.

If your actions are not working:

* Test that the connector works on the connector page
* Check the query returns hits
* Change alert to "Every time alert is active"





